---
Title: Design Challenge 5
date: 2017-09-08
---
# De dag van de kick off

Op donderdag 7 september was de kick-off van Design Challenge 5. Zelf was ik nog een beetje chagrijnig aangezien ik de vorige avond te horen had gekregen dat helaas mijn team niet was geselecteerd voor het open project. Bij de kick-off kregen we te horen dat de opdracht voor Unique zou zijn. Na een korte voorstel ronde van de opdrachtgevers werd onze opdracht geformuleerd: **Ontwerp een totaalconcept voor het uitzendbureau, waarin experience en service centraal staan.** 

Na de pauze werd het tijd om groepjes te maken. Ik besloot om bij mijn open project groepje te blijven met Marjolein en Michael en ook Corné werd hieraan toegevoegd. We kregen te horen dat we een Plan van Aanpak moesten gaan schrijven in de week erna. We zijn alvast begonnen met het maken van een scrum bord en het bedenken van een team naam. Verder hebben we die dag niet veel gedaan. 

